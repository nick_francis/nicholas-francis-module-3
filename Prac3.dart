void main() {
  var app = AppInfo(name: 'Shyft', category: 'Finance', winningYear: 2017, developer: "Standard Bank");
  print(app.capsLock());
  print(app.announcement());
}

class AppInfo {
  
  AppInfo({this.name, this.category, this.winningYear, this.developer});
  String? name;
  String? category;
  int? winningYear;
  String? developer;
  
  String? capsLock () =>
    
    "$name".toUpperCase();
    
 announcement (){
  
  print('Winner: $name Category: $category, Winning Year: $winningYear, Developer: $developer');
}

}
